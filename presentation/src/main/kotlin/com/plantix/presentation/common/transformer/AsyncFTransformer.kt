package com.plantix.presentation.common.transformer

import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import com.plantix.domain.common.transformer.FTransformer
import org.reactivestreams.Publisher

/**
 * Created by Droider
 */
class AsyncFTransformer<T> : FTransformer<T>() {

    override fun apply(upstream: Flowable<T>): Publisher<T> =
        upstream.subscribeOn(Schedulers.io())
}