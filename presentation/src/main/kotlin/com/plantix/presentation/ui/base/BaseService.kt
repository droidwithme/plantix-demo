package com.plantix.presentation.ui.base

import dagger.android.DaggerService

/**
 * Created by Droider
 */
abstract class BaseService : DaggerService()