package com.plantix.presentation.ui.datalist

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.plantix.domain.entity.Entity
import com.plantix.presentation.R


class DataAdapter(var dataList: ArrayList<Entity.DataList>) :
    RecyclerView.Adapter<DataAdapter.MyViewHolder>() {
    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var colorName: TextView = view.findViewById(R.id.itemName)
        var itemColor: TextView = view.findViewById(R.id.itemColor)
        var itemColorBox: TextView = view.findViewById(R.id.itemColorBox)
        var itemYear: TextView = view.findViewById(R.id.itemYear)
    }

    fun submitData(data: List<Entity.DataList>) {
        this.dataList.clear()
        this.dataList.addAll(data)
        notifyDataSetChanged()
    }

    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_datalist, parent, false)
        return MyViewHolder(itemView)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val data = dataList[position]
        holder.colorName.text = data.name
        holder.itemColor.text = data.color
        holder.itemYear.text = data.year
        holder.itemColorBox.setBackgroundColor(Color.parseColor(data.color))
    }

    override fun getItemCount(): Int {
        return dataList.size
    }
}