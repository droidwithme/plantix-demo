package com.plantix.presentation.ui.login

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.plantix.domain.common.ResultState
import com.plantix.domain.entity.Entity
import com.plantix.presentation.R
import com.plantix.presentation.common.extension.observe
import com.plantix.presentation.ui.MainActivity
import com.plantix.presentation.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_login.*
import javax.inject.Inject

/**
 * Created by Droider.
 */
class LoginFragment : BaseFragment() {

    val TAG = LoginFragment::class.simpleName

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: LoginViewModel by lazy {
        logMessage(TAG!!, "providing loginViewModel")
        ViewModelProviders.of(this, viewModelFactory).get(LoginViewModel::class.java)
    }

    private fun onLogin(resultState: ResultState<Entity.LoginResponse>) {
        logMessage(TAG!!, "onLogin($resultState)")
        hideLoading()
        when (resultState) {
            is ResultState.Success ->
                logMessage(TAG, "onResult.Success")
            is ResultState.Error ->
                logMessage(TAG, "onResult.Error->${resultState.throwable.message}")
        }
    }


    @SuppressLint("CheckResult")
    private fun initView() {
        logMessage(TAG!!, "initView()")
        loginButtonLogin.setOnClickListener {
            logMessage(TAG, "on Login click")
            /*  val email = editTextEmail.text.toString()
              val password = editTextPasword.text.toString()
              viewModel.login(Entity.LoginQ(email, password))*/
            findNavController().navigate(R.id.loginToDataList)

        }

    }

    override fun onStart() {
        super.onStart()
        logMessage(TAG!!, "onStart()")
        (activity as MainActivity).showToolBar("Login")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        logMessage(TAG!!, "onCreateView()")
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        logMessage(TAG!!, "onViewCreated()")
        initView()
        observe(viewModel.loginCall, ::onLogin)
    }


}