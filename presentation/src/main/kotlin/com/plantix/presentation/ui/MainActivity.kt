package com.plantix.presentation.ui

import android.os.Bundle
import android.view.View
import com.plantix.presentation.R
import com.plantix.presentation.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*

/**
 * Created by Droider.
 */
class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        showToolBar(null)
    }

    override fun getNavControllerId(): Int = R.id.activityMainHomeHostFragment

    fun showToolBar(title: String?) {
        setSupportActionBar(toolbar)
        toolbar.visibility = View.VISIBLE
        supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbar_title.text = title
    }

    fun hideToolBar() {
        toolbar.visibility = View.GONE
    }

}