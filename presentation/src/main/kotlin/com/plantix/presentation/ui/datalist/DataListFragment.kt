package com.plantix.presentation.ui.datalist

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import com.plantix.domain.common.ResultState
import com.plantix.domain.entity.Entity
import com.plantix.presentation.R
import com.plantix.presentation.common.extension.observe
import com.plantix.presentation.ui.MainActivity
import com.plantix.presentation.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_datalist.*
import javax.inject.Inject

/**
 * Created by Droider.
 */
class DataListFragment : BaseFragment() {

    val TAG = DataListFragment::class.simpleName

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private var isLoading = false
    lateinit var adapter: DataAdapter


    private val viewModel: DataListViewModel by lazy {
        logMessage(TAG!!, "providing loginViewModel")
        ViewModelProviders.of(this, viewModelFactory).get(DataListViewModel::class.java)
    }


    private fun showDataList(albums: ResultState<List<Entity.DataList>>) {
        logMessage(TAG!!, "showAlbums($albums)")
        hideLoading()
        when (albums) {
            is ResultState.Success -> {

                adapter.submitData(albums.data)
            }
            is ResultState.Error -> {

                Toast.makeText(activity, albums.throwable.message, Toast.LENGTH_SHORT).show()
                adapter.submitData(ArrayList())
            }
            is ResultState.Loading -> {
                adapter.submitData(ArrayList())
            }
        }
        isLoading = false

    }


    @SuppressLint("CheckResult")
    private fun initView() {
        logMessage(TAG!!, "initView()")
        fragmentDataList.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )
        fragmentDataList.setHasFixedSize(true)
        adapter = DataAdapter(ArrayList())
        fragmentDataList.adapter = adapter
        showLoading()
    }

    override fun onStart() {
        super.onStart()
        logMessage(TAG!!, "onStart()")
        (activity as MainActivity).showToolBar("List Of Colors")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        logMessage(TAG!!, "onCreateView()")
        return inflater.inflate(R.layout.fragment_datalist, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        logMessage(TAG!!, "onViewCreated()")
        initView()
        observe(viewModel.loginCall, ::showDataList)
        viewModel.getList()
    }


}