package com.plantix.presentation.ui.splash

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.plantix.presentation.R
import com.plantix.presentation.ui.MainActivity
import com.plantix.presentation.ui.base.BaseFragment
import javax.inject.Inject


/**
 * Created by Droider.
 */
class SplashFragment : BaseFragment() {

    val TAG = SplashFragment::class.simpleName

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: SplashViewModel by lazy {
        logMessage(TAG!!, "providing SplashViewModel by lazy")

        ViewModelProviders.of(this, viewModelFactory).get(SplashViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        logMessage(TAG!!, "onCreate()")
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        logMessage(TAG!!, "onViewCreated()")
        Handler().postDelayed({
            findNavController().navigate(R.id.splashToLogin)
        }, 3000)

    }

    override fun onStart() {
        super.onStart()
        logMessage(TAG!!, "onStart()")
        (activity as MainActivity).hideToolBar()
    }
}