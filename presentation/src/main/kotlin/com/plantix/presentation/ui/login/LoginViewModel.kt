package com.plantix.presentation.ui.login

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import io.reactivex.disposables.Disposable
import com.plantix.domain.common.ResultState
import com.plantix.domain.entity.Entity
import com.plantix.domain.usecase.login.LoginUseCase
import com.plantix.presentation.common.OperationLiveData
import com.plantix.presentation.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by Droider
 */
class LoginViewModel @Inject constructor(private val loginUseCase: LoginUseCase) :
    BaseViewModel() {
    val TAG = LoginViewModel::class.simpleName

    private var tempDispossable: Disposable? = null

    private val loginQ = MutableLiveData<Entity.LoginQ>()

    val loginCall: LiveData<ResultState<Entity.LoginResponse>> =
        Transformations.switchMap(loginQ) {
            OperationLiveData<ResultState<Entity.LoginResponse>> {
                Log.e(TAG, "executing usecase")
                if (tempDispossable?.isDisposed != true)
                    tempDispossable?.dispose()
                tempDispossable = loginUseCase.login(it).subscribe { resultState ->
                    Log.e(TAG, "posting values from use case$resultState")
                    postValue((resultState))
                }
                tempDispossable?.track()
            }
        }


    fun login(entity: Entity.LoginQ) {
        Log.e(TAG, "login($entity)")
        loginQ.postValue(entity)
    }

}