package com.plantix.presentation.ui.datalist

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import io.reactivex.disposables.Disposable
import com.plantix.domain.common.ResultState
import com.plantix.domain.entity.Entity
import com.plantix.domain.usecase.datalist.DataListUseCase
import com.plantix.presentation.common.OperationLiveData
import com.plantix.presentation.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by Droider
 */
class DataListViewModel @Inject constructor(private val dataListUseCase: DataListUseCase) :
    BaseViewModel() {
    val TAG = DataListViewModel::class.simpleName

    private var tempDispossable: Disposable? = null
    private val fetch = MutableLiveData<String>()


    val loginCall: LiveData<ResultState<List<Entity.DataList>>> =
        Transformations.switchMap(fetch) {
            OperationLiveData<ResultState<List<Entity.DataList>>> {
                Log.e(TAG, "executing usecase")
                if (tempDispossable?.isDisposed != true)
                    tempDispossable?.dispose()
                tempDispossable = dataListUseCase.getData().subscribe { resultState ->
                    Log.e(TAG, "posting values from use case$resultState")
                    postValue((resultState))
                }
                tempDispossable?.track()
            }
        }


    fun getList() {
        fetch.postValue("")
    }

}