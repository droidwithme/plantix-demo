package com.plantix.presentation.ui.home

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.paging.PagedList
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.plantix.domain.common.ResultState
import com.plantix.domain.entity.Entity
import com.plantix.presentation.R
import com.plantix.presentation.common.extension.applyIoScheduler
import com.plantix.presentation.common.extension.observe
import com.plantix.presentation.ui.MainActivity
import com.plantix.presentation.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_home.*
import javax.inject.Inject

/**
 * Created by Droider.
 */
class HomeFragment : BaseFragment(), SwipeRefreshLayout.OnRefreshListener {
    val TAG = HomeFragment::class.simpleName

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private var isLoading = false

    private val adapter: AlbumListAdapter by lazy {
        AlbumListAdapter()
    }

    private val viewModel: HomeViewModel by lazy {
        logMessage(TAG!!, "providing view model")
        ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)
    }

    private fun onAlbumDeleted(resultState: ResultState<Int>) {
        logMessage(TAG!!, "onAlbumDeleted")
        when (resultState) {
            is ResultState.Success -> Toast.makeText(
                activity,
                "album ${resultState.data} deleted",
                Toast.LENGTH_SHORT
            ).show()
            is ResultState.Error -> Toast.makeText(
                activity,
                resultState.throwable.message,
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun showAlbums(albums: ResultState<PagedList<Entity.Album>>) {
        logMessage(TAG!!, "showAlbums($albums)")
        when (albums) {
            is ResultState.Success -> {
                hideLoading()
                adapter.submitList(albums.data)
            }
            is ResultState.Error -> {
                hideLoading()
                Toast.makeText(activity, albums.throwable.message, Toast.LENGTH_SHORT).show()
                adapter.submitList(albums.data)
            }
            is ResultState.Loading -> {
                adapter.submitList(albums.data)
            }
        }
        isLoading = false
        fragmentHomeSwp.isRefreshing = false
    }

    @SuppressLint("CheckResult")
    private fun initView() {
        logMessage(TAG!!, "initView()")
        fragmentHomeSwp.isRefreshing = true
        fragmentHomeSwp.setOnRefreshListener(this)
        fragmentHomeRcyMain.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )
        fragmentHomeRcyMain.setHasFixedSize(true)
        fragmentHomeRcyMain.adapter = adapter

        adapter.albumItemClickEvent.applyIoScheduler().subscribe { it ->
            logMessage(TAG, "applyToScheduler")
            viewModel.deleteAlbum(it)
        }

        showLoading()
    }

    override fun onStart() {
        super.onStart()
        logMessage(TAG!!, "onStart()")
        (activity as MainActivity).showToolBar("List Of Users")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        logMessage(TAG!!, "onCreateView()")
        return inflater.inflate(R.layout.fragment_home, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        logMessage(TAG!!, "onViewCreated()")
        initView()
        observe(viewModel.albumsLiveData, ::showAlbums)
        observe(viewModel.deletedAlbumLiveData, ::onAlbumDeleted)
        viewModel.getAlbums()
    }

    override fun onRefresh() {
    }
}