package com.plantix.presentation.ui.home

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.PagedList
import io.reactivex.disposables.Disposable
import com.plantix.domain.common.ResultState
import com.plantix.domain.entity.Entity
import com.plantix.domain.usecase.album.GetAlbumsUseCase
import com.plantix.presentation.common.OperationLiveData
import com.plantix.presentation.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by Droider.
 */
class HomeViewModel @Inject constructor(private val getAlbumsUseCase: GetAlbumsUseCase) :
    BaseViewModel() {
    val TAG = HomeViewModel::class.simpleName
    private var tempDispossable: Disposable? = null

    //var page = 1
    private val albumToBeDeleted = MutableLiveData<Entity.Album>()

    //private val pageLiveData by lazy { MutableLiveData<String>()/*.defaultValue(1)*/ }
    //val pageNumberLiveData by lazy { MutableLiveData<Int>().defaultValue(1) }
    private val fetch = MutableLiveData<String>()

    @SuppressLint("CheckResult")
    val deletedAlbumLiveData: LiveData<ResultState<Int>> =
        Transformations.switchMap(albumToBeDeleted) {
            OperationLiveData<ResultState<Int>> {
                Log.e(TAG, "executing use case")
                getAlbumsUseCase.deleteAlbum(it).toFlowable().subscribe { resultState ->
                    Log.e(TAG, "posting response from usecase")
                    postValue(resultState)
                }
            }
        }

    fun deleteAlbum(album: Entity.Album) {
        Log.e(TAG!!, "deleteAlbum($album)")
        albumToBeDeleted.postValue(album)
    }

    val albumsLiveData: LiveData<ResultState<PagedList<Entity.Album>>> =
        Transformations.switchMap(fetch) {
            OperationLiveData<ResultState<PagedList<Entity.Album>>> {
                if (tempDispossable?.isDisposed != true)
                    tempDispossable?.dispose()
                tempDispossable = getAlbumsUseCase.getAlbums().subscribe { resultState ->
                    postValue((resultState))
                }
                tempDispossable?.track()
            }
        }


    fun getAlbums() {
        fetch.postValue("")
        //page++
    }
}