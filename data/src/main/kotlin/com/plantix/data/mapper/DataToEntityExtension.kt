package com.plantix.data.mapper

import com.plantix.data.db.album.AlbumData
import com.plantix.data.db.login.LoginData
import com.plantix.domain.entity.Entity

/**
 * Created by Droider
 */
/**
 * Extension class to map album data to album entity
 */
fun AlbumData.Album.map() = Entity.Album(
    id = id,
    userId = userId,
    title = title
)

fun LoginData.Login.map() = Entity.LoginResponse(
    id, token
)

