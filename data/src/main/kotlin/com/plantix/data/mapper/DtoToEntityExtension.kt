package com.plantix.data.mapper

import com.plantix.data.api.AlbumApi
import com.plantix.data.api.LoginApi
import com.plantix.domain.entity.Entity

/**
 * Created by Droider
 */
/**
 * Extension class to map album dto to album entity
 */
fun AlbumApi.Dto.Album.map() = Entity.Album(
    id = id,
    userId = userId,
    title = title
)

fun LoginApi.Dto.Login.map() = Entity.LoginResponse(
    id, token
)

fun LoginApi.Dto.Users.map() = Entity.DataList(
    id, name.substring(0, 1).toUpperCase() + name.substring(1), year, color
)