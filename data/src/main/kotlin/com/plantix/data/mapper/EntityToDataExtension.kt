package com.plantix.data.mapper

import com.plantix.data.db.album.AlbumData
import com.plantix.data.db.login.LoginData
import com.plantix.domain.entity.Entity

/**
 * Created by Droider
 */
/**
 * Extension class to map album entity to album data
 */
fun Entity.Album.map() = AlbumData.Album(
    id = id,
    userId = userId,
    title = title
)

fun Entity.LoginResponse.map() = LoginData.Login(
    id = id,
    token = token
)

