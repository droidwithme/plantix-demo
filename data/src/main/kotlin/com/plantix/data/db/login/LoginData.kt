package com.plantix.data.db.login

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Droider
 */
/**
 * Album data class
 */
sealed class LoginData {

    @Entity(tableName = "user_table")
    data class Login(
        @ColumnInfo(name = "id") @PrimaryKey(autoGenerate = false) val id: Long,
        @ColumnInfo(name = "token") val token: String
    ) : LoginData()
}