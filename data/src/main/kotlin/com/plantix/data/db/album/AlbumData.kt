package com.plantix.data.db.album

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Droider
 */
/**
 * Album data class
 */
sealed class AlbumData {

    @Entity(tableName = "album_table")
    data class Album(
        @ColumnInfo(name = "id") @PrimaryKey(autoGenerate = false) val id: Long,
        @ColumnInfo(name = "user_id") val userId: Long,
        @ColumnInfo(name = "title") val title: String
    ) : AlbumData()
}