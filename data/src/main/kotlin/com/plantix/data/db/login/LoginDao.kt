package com.plantix.data.db.login

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import io.reactivex.Flowable
import com.plantix.data.db.BaseDao

/**
 * Created by Droider
 */
/**
 * Album dao
 */
@Dao
interface LoginDao : BaseDao<LoginData.Login> {

    @Query("SELECT * FROM user_table WHERE id = :id")
    override fun select(id: Long): Flowable<LoginData.Login>


    @Query("SELECT * FROM user_table ORDER BY id")
    override fun selectAllPaged(): DataSource.Factory<Int, LoginData.Login>

    @Query("DELETE FROM user_table")
    override fun truncate()

    @Transaction
    fun replace(login: List<LoginData.Login>) {
        val firstId: Long = login.firstOrNull()?.id ?: run {
            0L
        }

        val lastId = login.lastOrNull()?.id ?: run {
            Long.MAX_VALUE
        }

        deleteRange(firstId, lastId)
        insert(login)
    }

    @Query("DELETE FROM user_table WHERE id BETWEEN :firstId AND :lastId")
    fun deleteRange(firstId: Long, lastId: Long)
}