package com.plantix.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.plantix.data.db.album.AlbumDao
import com.plantix.data.db.album.AlbumData
import com.plantix.data.db.login.LoginDao
import com.plantix.data.db.login.LoginData

/**
 * Created by Droider
 */
/**
 * Database class with all of its dao classes
 */
@Database(
    entities = [AlbumData.Album::class, LoginData.Login::class],
    version = 1,
    exportSchema = false
)
abstract class MyDatabase : RoomDatabase() {

    abstract fun albumDao(): AlbumDao

    abstract fun loginDao(): LoginDao
}