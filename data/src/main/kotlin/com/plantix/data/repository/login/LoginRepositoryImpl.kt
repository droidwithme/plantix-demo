package com.plantix.data.repository.login

import android.annotation.SuppressLint
import android.util.Log
import com.plantix.data.common.extension.applyIoScheduler
import com.plantix.data.datasource.login.LoginApiDataSource
import com.plantix.data.datasource.login.LoginDatabaseDataSource
import com.plantix.data.repository.BaseRepositoryImpl
import com.plantix.domain.common.ResultState
import com.plantix.domain.entity.Entity
import com.plantix.domain.repository.login.LoginRepository
import io.reactivex.Single

/**
 * Created by Droider
 */
/**
 * Album repository implementation
 */
class LoginRepositoryImpl(
    private val apiDataSource: LoginApiDataSource,
    private val loginDatabaseDataSource: LoginDatabaseDataSource
) : BaseRepositoryImpl<Entity.LoginResponse>(), LoginRepository {
    val TAG = LoginRepositoryImpl::class.simpleName

    @SuppressLint("CheckResult")
    override fun login(loginQ: Entity.LoginQ): Single<ResultState<Entity.LoginResponse>> {
        val data = apiDataSource.login(loginQ)

        return data.applyIoScheduler().map { loginResponse ->
            Log.e(TAG, "mapping data-> $loginResponse")
            ResultState.Success(loginResponse) as ResultState<Entity.LoginResponse>
        }.onErrorReturn { e ->
            Log.e(TAG, "onErrorReturn-> ${e.message}")
            ResultState.Error(e, null)
        }
    }


}