package com.plantix.data.repository.datalist

import android.util.Log
import io.reactivex.Single
import com.plantix.data.common.extension.applyIoScheduler
import com.plantix.data.datasource.datalist.DataListApiDataSource
import com.plantix.data.repository.BaseRepositoryImpl
import com.plantix.domain.common.ResultState
import com.plantix.domain.entity.Entity
import com.plantix.domain.repository.datalist.DataListRepository

/**
 * Created by Droider
 */
/**
 * Album repository implementation
 */
class DataListRepositoryImpl(
    private val apiDataSource: DataListApiDataSource
) : BaseRepositoryImpl<Entity.DataList>(), DataListRepository {
    val TAG = DataListRepositoryImpl::class.simpleName


    override fun getDataList(): Single<ResultState<List<Entity.DataList>>> {
        val data = apiDataSource.getData()
        return data.applyIoScheduler().map { loginResponse ->
            Log.e(TAG, "mapping data-> $loginResponse")
            ResultState.Success(loginResponse) as ResultState<List<Entity.DataList>>
        }.onErrorReturn { e ->
            Log.e(TAG, "onErrorReturn-> ${e.message}")
            ResultState.Error(e, null)
        }
    }


}