package com.plantix.data.datasource.datalist

import android.annotation.SuppressLint
import android.util.Log
import io.reactivex.Single
import com.plantix.data.datasource.BaseDataSource
import com.plantix.domain.common.ResultState
import com.plantix.domain.entity.Entity
import com.plantix.domain.usecase.datalist.DataListUseCase

/**
 * Created by Droider.
 */
@SuppressLint("CheckResult")
fun getData(
    apiSource: DataListUseCase,
    onResult: (result: ResultState<List<Entity.DataList>>) -> Unit
) {
    Log.e("LoginApiDataSource", "login()")
    apiSource.getData()
        .subscribe({ data ->
            Log.e("LoginApiDataSource", "apiSource.login($data)")
            onResult(data)
        }, { throwable ->
            Log.e("LoginApiDataSource", "apiSource.login($throwable)")
            onResult(ResultState.Error(throwable, null))
        })
}

/**
 * Album network data source
 */
interface DataListApiDataSource : BaseDataSource {
    /**
     * Get all of albums from network
     */
    fun getData(): Single<List<Entity.DataList>>
}