package com.plantix.data.datasource.login

import android.annotation.SuppressLint
import android.util.Log
import io.reactivex.Single
import com.plantix.data.datasource.BaseDataSource
import com.plantix.domain.common.ResultState
import com.plantix.domain.entity.Entity

/**
 * Created by Droider
 */
@SuppressLint("CheckResult")
fun login(
    logingQ: Entity.LoginQ,
    apiSource: LoginApiDataSource,
    onResult: (result: ResultState<Entity.LoginResponse>) -> Unit
) {
    Log.e("LoginApiDataSource", "login()")
    apiSource.login(logingQ)
        .subscribe({ data ->
            Log.e("LoginApiDataSource", "apiSource.login($data)")
            onResult(ResultState.Success(data))
        }, { throwable ->
            Log.e("LoginApiDataSource", "apiSource.login($throwable)")
            onResult(ResultState.Error(throwable, null))
        })
}

/**
 * Album network data source
 */
interface LoginApiDataSource : BaseDataSource {
    /**
     * Get all of albums from network
     */
    fun login(logingQ: Entity.LoginQ): Single<Entity.LoginResponse>
}