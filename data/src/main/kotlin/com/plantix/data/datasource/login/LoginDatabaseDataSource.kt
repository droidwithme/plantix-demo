package com.plantix.data.datasource.login

import androidx.paging.DataSource
import com.plantix.data.datasource.BaseDataSource
import com.plantix.domain.entity.Entity

/**
 * Created by Droider
 */
/**
 * Album database data source
 */
interface LoginDatabaseDataSource : BaseDataSource {

    /**
     * Get all of albums from database implementation
     */
    fun getLoggedInUser(): DataSource.Factory<Int, Entity.LoginResponse>

    /**
     * Persist all of local data in local database
     */
    fun saveLogin(login: Entity.LoginResponse)

    //fun deleteAlbum(album: Entity.Album): Single<Int>
}