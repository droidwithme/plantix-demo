package com.plantix.data.datasource.datalist

import io.reactivex.Single
import com.plantix.data.api.LoginApi
import com.plantix.data.mapper.map
import com.plantix.domain.entity.Entity

/**
 * Created by Droider
 */
/**
 * Album network data source implementation
 */
class DataListApiDataSourceImpl(private val api: LoginApi) : DataListApiDataSource {
    override fun getData(): Single<List<Entity.DataList>> =
        api.getUserList().map { item -> item.data.map { it.map() } }
}