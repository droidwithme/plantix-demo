package com.plantix.data.datasource.login

import io.reactivex.Single
import com.plantix.data.api.LoginApi
import com.plantix.data.mapper.map
import com.plantix.domain.entity.Entity

/**
 * Created by Droider
 */
/**
 * Album network data source implementation
 */
class LoginApiDataSourceImpl(private val api: LoginApi) : LoginApiDataSource {
    override fun login(logingQ: Entity.LoginQ): Single<Entity.LoginResponse> {
        return api.login(logingQ).map { it.map() }
    }
}