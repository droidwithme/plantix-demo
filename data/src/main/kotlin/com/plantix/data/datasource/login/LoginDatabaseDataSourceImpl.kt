package com.plantix.data.datasource.login

import android.util.Log
import androidx.paging.DataSource
import com.plantix.data.db.login.LoginDao
import com.plantix.data.mapper.map
import com.plantix.domain.entity.Entity
import java.util.concurrent.Executor

/**
 * Created by Droider
 */
/**
 * Album database data source implementation
 */
class LoginDatabaseDataSourceImpl(
    private val loginDao: LoginDao,
    private val ioExecutor: Executor
) : LoginDatabaseDataSource {
    override fun getLoggedInUser(): DataSource.Factory<Int, Entity.LoginResponse> {
        return loginDao.selectAllPaged()
            .map { it.map() }
    }

    override fun saveLogin(login: Entity.LoginResponse) {
        ioExecutor.execute {
            Log.e("LoginDbImpl", "saveLogin($login)")
            loginDao.insert(login.map())
        }
    }


}