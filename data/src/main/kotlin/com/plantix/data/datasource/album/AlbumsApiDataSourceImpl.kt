package com.plantix.data.datasource.album

import io.reactivex.Single
import com.plantix.data.api.AlbumApi
import com.plantix.data.common.extension.applyIoScheduler
import com.plantix.data.mapper.map
import com.plantix.domain.entity.Entity

/**
 * Created by Droider
 */
/**
 * Album network data source implementation
 */
class AlbumsApiDataSourceImpl(private val api: AlbumApi) : AlbumsApiDataSource {

    /**
     * Get all of albums from network implementation
     */
    override fun getAlbums(page: Int, pageSize: Int): Single<List<Entity.Album>> =
        api.getAlbums(page, pageSize)
            .applyIoScheduler()
            .map { item -> item.map { it.map() } }
}