package com.plantix.data.api

import com.google.gson.annotations.SerializedName
import io.reactivex.Single
import com.plantix.domain.entity.Entity
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface LoginApi {

    @POST("register")
    fun login(@Body entity: Entity.LoginQ): Single<Dto.Login>


    @GET("unknown")
    fun getUserList(): Single<Dto.UserList>

    sealed class Dto {

        data class Login(
            @SerializedName("id") val id: Long,
            @SerializedName("token") val token: String
        ) : Dto()

        data class UserList(
            val data: ArrayList<Users>
        ) : Dto()

        data class Users(
            @SerializedName("id") val id: Long,
            @SerializedName("name") val name: String,
            @SerializedName("year") val year: String,
            @SerializedName("color") val color: String
        ) : Dto()
    }
}