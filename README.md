# Plantix-Demo

A simple Android application to show the color list with its graphical propery.
MVVM design pattern usage with following Clean Architecture approach.

[APK FILE](https://gitlab.com/droidwithme/plantix-demo/-/blob/master/app-debug.apk)

## Features

- 100% Kotlin-only.
- Following Clean Architecture approach.
- Following MVVM Architectural Design Pattern.
- Using Navigation Component.

## Things to be done
   
1. Testcases
2. CI/CD Integretion

## [Backend Service](https://reqres.in/)

## Repository's basic info
Repository have 4 Modules all of them are loose coupled. 
The repository written in kotlin.
Using the latest framework APIs
There is no hard dependency.


- - App
This is the entry point of the app. and contains Dagger operations.
Providing the dependencies to the presentaion, domain and data.

- - Data
Executing the network or database operations. and emits the data to domain.

- - Domain
Containing the usecases and mappers. handling the transport of data from presentaion to data and vice versa.
providing the repository to data layer to implement it.

- - Presentaion
contains the splash, login and color list screen.
Accesing dummuy api for login and dummy api to retrive the list of color.
App starts from Splash Fragment and leads to a login screen, where a dummy API implemented for login. 
Next screen is List of colors, That have a list of color name. Here is the API have
` {
            "id": 1,
            "name": "cerulean",
            "year": 2000,
            "color": "#98B2D1",
            "pantone_value": "15-4020"
        }`
The response have color name is the small case. 

According to my understanding, 
**The app should show first letter in uppercase while in respose it is in lowercase.**

**Inspired and took help from[From This Tutorial](https://www.raywenderlich.com/3595916-clean-architecture-tutorial-for-android-getting-started)**

## _Additional info:_
**Which technologies would you use for implementing the remote & local data source in a real application assumed our backend provides a REST API**
If I am understaing correctly this question is asking about the Backend technology to develop the API in.
IMO, Android developer is not depended on the tech or backend language, the devoper is only looks how the backend is providing the data. 
A good backend should provide the data in restful manner.

## Result
![Splash](splash.png)

## Login
[Reqres](https://reqres.in/) used for dummy login.
![Splash](login.png)

## Data List
[Reqres](https://reqres.in/) Requres's Random Api used to show the list of colors. Where it contains first char lower case. Converted the first char to uppercase.
Although the other fields have first letter capital while the email do not. So I capitalized the email's first char.
![Splash](data.png)

