package com.plantix.domain.entity

/**
 * Created by Droider
 */
/**
 * Album entity
 */
sealed class Entity {

    data class Album(
        val id: Long,
        val title: String,
        val userId: Long
    ) : Entity()

    data class LoginQ(
        val email: String,
        val password: String
    ) : Entity()

    data class LoginResponse(
        val id: Long,
        val token: String
    ) : Entity()

    data class DataList(
        val id: Long,
        val name: String,
        val year: String,
        val color: String
    ) : Entity()
}