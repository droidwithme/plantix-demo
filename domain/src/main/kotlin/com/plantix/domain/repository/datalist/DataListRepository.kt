package com.plantix.domain.repository.datalist

import io.reactivex.Single
import com.plantix.domain.common.ResultState
import com.plantix.domain.entity.Entity
import com.plantix.domain.repository.BaseRepository

/**
 * Created by Droider
 */
/**
 * Album repository
 */
interface DataListRepository : BaseRepository {

    /**
     * Perform
     */
    fun getDataList(): Single<ResultState<List<Entity.DataList>>>

}