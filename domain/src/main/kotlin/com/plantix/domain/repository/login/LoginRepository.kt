package com.plantix.domain.repository.login

import io.reactivex.Single
import com.plantix.domain.common.ResultState
import com.plantix.domain.entity.Entity
import com.plantix.domain.repository.BaseRepository

/**
 * Created by Droider
 */
/**
 * Album repository
 */
interface LoginRepository : BaseRepository {

    /**
     * Perform
     */
    fun login(loginQ: Entity.LoginQ): Single<ResultState<Entity.LoginResponse>>


}