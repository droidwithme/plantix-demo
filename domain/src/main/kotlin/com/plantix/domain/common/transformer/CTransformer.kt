package com.plantix.domain.common.transformer

import io.reactivex.CompletableTransformer

/**
 * Created by Droider
 */
/**
 * A transformer to io thread for completables.
 */
abstract class CTransformer : CompletableTransformer