package com.plantix.domain.common.transformer

import io.reactivex.FlowableTransformer

/**
 * Created by Droider
 */
/**
 * A transformer to io thread for flowables.
 */
abstract class FTransformer<T> : FlowableTransformer<T, T>