package com.plantix.domain.common.transformer

import io.reactivex.SingleTransformer

/**
 * Created by Droider
 */
/**
 * A transformer to io thread for singles.
 */
abstract class STransformer<T> : SingleTransformer<T, T>