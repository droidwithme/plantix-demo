package com.plantix.domain.usecase.datalist

import io.reactivex.Single
import com.plantix.domain.common.ResultState
import com.plantix.domain.entity.Entity
import com.plantix.domain.usecase.BaseUseCase

/**
 * Created by Droider
 */
/**
 * Album use case
 */
interface DataListUseCase : BaseUseCase {

    /**
     * Get all of albums use case
     */
    fun getData(): Single<ResultState<List<Entity.DataList>>>


}