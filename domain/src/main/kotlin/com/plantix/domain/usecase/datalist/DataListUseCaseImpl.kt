package com.plantix.domain.usecase.datalist

import androidx.paging.PagedList
import io.reactivex.Single
import com.plantix.domain.common.ResultState
import com.plantix.domain.common.transformer.FTransformer
import com.plantix.domain.common.transformer.STransformer
import com.plantix.domain.entity.Entity
import com.plantix.domain.repository.datalist.DataListRepository

/**
 * Created by Droider
 */
/**
 * Album use case implementation
 */
class DataListUseCaseImpl(
    private val transformerFlowable: FTransformer<ResultState<PagedList<Entity.DataList>>>,
    private val transformerSingle: STransformer<ResultState<List<Entity.DataList>>>,
    private val transformerSingleList: STransformer<ResultState<PagedList<Entity.DataList>>>,
    private val repository: DataListRepository
) : DataListUseCase {


    override fun getData(): Single<ResultState<List<Entity.DataList>>> =
        repository.getDataList().compose(transformerSingle)
}
