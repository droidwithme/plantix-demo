package com.plantix.domain.usecase.login

import io.reactivex.Single
import com.plantix.domain.common.ResultState
import com.plantix.domain.entity.Entity
import com.plantix.domain.usecase.BaseUseCase

/**
 * Created by Droider
 */
/**
 * Album use case
 */
interface LoginUseCase : BaseUseCase {

    /**
     * Get all of albums use case
     */
    fun login(loginQ: Entity.LoginQ): Single<ResultState<Entity.LoginResponse>>


}