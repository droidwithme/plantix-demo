package com.plantix.domain.usecase.login

import androidx.paging.PagedList
import io.reactivex.Single
import com.plantix.domain.common.ResultState
import com.plantix.domain.common.transformer.FTransformer
import com.plantix.domain.common.transformer.STransformer
import com.plantix.domain.entity.Entity
import com.plantix.domain.repository.login.LoginRepository

/**
 * Created by Droider
 */
/**
 * Album use case implementation
 */
class LoginUseCaseImpl(
    private val transformerFlowable: FTransformer<ResultState<PagedList<Entity.Album>>>,
    private val transformerSingle: STransformer<ResultState<Entity.LoginResponse>>,
    private val transformerSingleList: STransformer<ResultState<List<Entity.Album>>>,
    private val repository: LoginRepository
) : LoginUseCase {
    override fun login(loginQ: Entity.LoginQ): Single<ResultState<Entity.LoginResponse>> {
        return repository.login(loginQ)
    }


}