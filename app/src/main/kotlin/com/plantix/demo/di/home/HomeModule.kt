package com.plantix.demo.di.home

import dagger.Module
import dagger.Provides
import com.plantix.data.api.AlbumApi
import com.plantix.data.datasource.album.AlbumsApiDataSource
import com.plantix.data.datasource.album.AlbumsApiDataSourceImpl
import com.plantix.data.datasource.album.AlbumsDatabaseDataSource
import com.plantix.data.datasource.album.AlbumsDatabaseDataSourceImpl
import com.plantix.data.db.album.AlbumDao
import com.plantix.data.repository.album.AlbumsRepositoryImpl
import com.plantix.domain.repository.album.AlbumsRepository
import com.plantix.domain.usecase.album.GetAlbumsUseCase
import com.plantix.domain.usecase.album.GetAlbumsUseCaseImpl
import com.plantix.presentation.common.transformer.AsyncFTransformer
import com.plantix.presentation.common.transformer.AsyncSTransformer
import java.util.concurrent.Executors

/**
 * Created by Droider.
 */
@Module
class HomeModule {

    @Provides
    //@PerFragment
    fun provideDatabaseSource(albumDao: AlbumDao): AlbumsDatabaseDataSource =
        AlbumsDatabaseDataSourceImpl(albumDao, Executors.newSingleThreadExecutor())

    @Provides
    //@PerFragment
    fun provideApiSource(api: AlbumApi): AlbumsApiDataSource = AlbumsApiDataSourceImpl(api)

    @Provides
    //@PerFragment
    fun provideRepository(
        apiSource: AlbumsApiDataSource,
        databaseSource: AlbumsDatabaseDataSource
    ): AlbumsRepository {
        return AlbumsRepositoryImpl(apiSource, databaseSource)
    }

    @Provides
    //@PerFragment
    fun provideGetAlbumsUseCaseImpl(repository: AlbumsRepository): GetAlbumsUseCase =
        GetAlbumsUseCaseImpl(
            AsyncFTransformer(),
            AsyncSTransformer(),
            AsyncSTransformer(),
            repository
        )
}
