package com.plantix.demo.di.home

import dagger.Module
import dagger.android.ContributesAndroidInjector
import com.plantix.presentation.ui.home.HomeFragment

@Module
abstract class HomeFragmentModule {

    @ContributesAndroidInjector
    abstract fun homeFragment(): HomeFragment
}