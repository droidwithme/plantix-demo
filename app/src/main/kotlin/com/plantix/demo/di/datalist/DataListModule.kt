package com.plantix.demo.di.datalist

import dagger.Module
import dagger.Provides
import com.plantix.data.api.LoginApi
import com.plantix.data.datasource.datalist.DataListApiDataSource
import com.plantix.data.datasource.datalist.DataListApiDataSourceImpl
import com.plantix.data.repository.datalist.DataListRepositoryImpl
import com.plantix.domain.repository.datalist.DataListRepository
import com.plantix.domain.usecase.datalist.DataListUseCase
import com.plantix.domain.usecase.datalist.DataListUseCaseImpl
import com.plantix.presentation.common.transformer.AsyncFTransformer
import com.plantix.presentation.common.transformer.AsyncSTransformer

/**
 * Created by Droider.
 */
@Module
class DataListModule {


    @Provides
    //@PerFragment
    fun provideApiSource(api: LoginApi): DataListApiDataSource = DataListApiDataSourceImpl(api)

    @Provides
    //@PerFragment
    fun provideRepository(
        apiDataSource: DataListApiDataSource
    ): DataListRepository {
        return DataListRepositoryImpl(apiDataSource)
    }

    @Provides
    //@PerFragment
    fun provideDataListUseCaseImpl(repository: DataListRepository): DataListUseCase =
        DataListUseCaseImpl(
            AsyncFTransformer(),
            AsyncSTransformer(),
            AsyncSTransformer(),
            repository
        )
}
