package com.plantix.demo.di.splash

import dagger.Module
import dagger.android.ContributesAndroidInjector
import com.plantix.presentation.ui.splash.SplashFragment

@Module
abstract class SplashFragmentModule {

    @ContributesAndroidInjector
    abstract fun splashFragment(): SplashFragment
}