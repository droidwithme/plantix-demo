package com.plantix.demo.di.component

import com.plantix.demo.MyApplication
import com.plantix.demo.di.AppModule
import com.plantix.demo.di.MainModule
import com.plantix.demo.di.ViewModelModule
import com.plantix.demo.di.datalist.DataListModule
import com.plantix.demo.di.home.HomeModule
import com.plantix.demo.di.login.LoginModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by Droider
 */
@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ViewModelModule::class,
        AppModule::class,
        MainModule::class,
        HomeModule::class,
        LoginModule::class,
        DataListModule::class
    ]
)
interface AppComponent : AndroidInjector<MyApplication> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<MyApplication>()
}