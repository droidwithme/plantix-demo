package com.plantix.demo.di.annotations

import javax.inject.Qualifier

/**
 * Created by Droider
 */
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ApplicationContext