package com.plantix.demo.di.annotations

import javax.inject.Scope

/**
 * Created by Droider
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerActivity