package com.plantix.demo.di.login

import dagger.Module
import dagger.Provides
import com.plantix.data.api.LoginApi
import com.plantix.data.datasource.login.LoginApiDataSource
import com.plantix.data.datasource.login.LoginApiDataSourceImpl
import com.plantix.data.datasource.login.LoginDatabaseDataSource
import com.plantix.data.datasource.login.LoginDatabaseDataSourceImpl
import com.plantix.data.db.login.LoginDao
import com.plantix.data.repository.login.LoginRepositoryImpl
import com.plantix.domain.repository.login.LoginRepository
import com.plantix.domain.usecase.login.LoginUseCase
import com.plantix.domain.usecase.login.LoginUseCaseImpl
import com.plantix.presentation.common.transformer.AsyncFTransformer
import com.plantix.presentation.common.transformer.AsyncSTransformer
import java.util.concurrent.Executors

/**
 * Created by Droider.
 */
@Module
class LoginModule {

    @Provides
    //@PerFragment
    fun provideDatabaseSource(loginDao: LoginDao): LoginDatabaseDataSource =
        LoginDatabaseDataSourceImpl(loginDao, Executors.newSingleThreadExecutor())


    @Provides
    //@PerFragment
    fun provideApiSource(api: LoginApi): LoginApiDataSource = LoginApiDataSourceImpl(api)

    @Provides
    //@PerFragment
    fun provideRepository(
        apiDataSource: LoginApiDataSource,
        loginDatabaseDataSource: LoginDatabaseDataSource
    ): LoginRepository {
        return LoginRepositoryImpl(apiDataSource, loginDatabaseDataSource)
    }

    @Provides
    //@PerFragment
    fun provideGetAlbumsUseCaseImpl(repository: LoginRepository): LoginUseCase =
        LoginUseCaseImpl(
            AsyncFTransformer(),
            AsyncSTransformer(),
            AsyncSTransformer(),
            repository
        )
}
