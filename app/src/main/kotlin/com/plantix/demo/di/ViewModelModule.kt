package com.plantix.demo.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import com.plantix.demo.di.annotations.ViewModelKey
import com.plantix.presentation.ui.datalist.DataListViewModel
import com.plantix.presentation.ui.home.HomeViewModel
import com.plantix.presentation.ui.login.LoginViewModel
import com.plantix.presentation.ui.splash.SplashViewModel

/**
 * Created by Droider
 */
@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    internal abstract fun bindHomeViewModel(viewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    internal abstract fun bindSplashViewModel(viewModel: SplashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    internal abstract fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DataListViewModel::class)
    internal abstract fun bindDataListViewModel(viewModel: DataListViewModel): ViewModel

}