package com.plantix.demo.di.login

import dagger.Module
import dagger.android.ContributesAndroidInjector
import com.plantix.presentation.ui.login.LoginFragment

@Module
abstract class LoginFragmentModule {

    @ContributesAndroidInjector
    abstract fun loginFragment(): LoginFragment
}