package com.plantix.demo.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import com.plantix.demo.di.datalist.DataListFragmentModule
import com.plantix.demo.di.home.HomeFragmentModule
import com.plantix.demo.di.login.LoginFragmentModule
import com.plantix.demo.di.splash.SplashFragmentModule
import com.plantix.presentation.ui.MainActivity

/**
 * Created by Droider
 */
@Module(includes = [HomeFragmentModule::class, SplashFragmentModule::class, LoginFragmentModule::class, DataListFragmentModule::class])
abstract class MainModule {

    //@PerActivity
    @ContributesAndroidInjector
    abstract fun get(): MainActivity
}