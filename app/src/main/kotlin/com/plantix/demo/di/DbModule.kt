package com.plantix.demo.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import com.plantix.data.db.MyDatabase
import com.plantix.demo.di.annotations.ApplicationContext
import javax.inject.Singleton

/**
 * Created by Droider
 */
@Module
class DbModule {

    @Singleton
    @Provides
    fun provideMyDatabase(@ApplicationContext context: Context) =
        Room.databaseBuilder(context, MyDatabase::class.java, "mydatabase")
            .build()

    @Singleton
    @Provides
    fun provideAlbumDao(myDatabase: MyDatabase) = myDatabase.albumDao()

    @Singleton
    @Provides
    fun provideLoginDao(myDatabase: MyDatabase) = myDatabase.loginDao()
}