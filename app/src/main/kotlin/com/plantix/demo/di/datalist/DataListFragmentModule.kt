package com.plantix.demo.di.datalist

import dagger.Module
import dagger.android.ContributesAndroidInjector
import com.plantix.presentation.ui.datalist.DataListFragment

@Module
abstract class DataListFragmentModule {

    @ContributesAndroidInjector
    abstract fun dataListFragment(): DataListFragment
}